
const { MongoClient, ServerApiVersion } = require('mongodb');
const uri = "mongodb+srv://root:mozark22@cluster0.bqf81.mongodb.net/testAnalytics?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });

const connectMongoDb =  () => {
    return new Promise((resolve, reject) => {
        client.connect()
            .then(() => resolve())
            .catch(err => {console.log("Connection error : ", err) ; reject(err)}) 
    })
}

const dashboardCollection = async ()=>{
    
    const collection = client.db("testAnalytics").collection("test_analysis_report");
    console.log("Collection : ", collection.collectionName);
    return collection;
}  

const readAnalytics = (collection, uuid) => {

    return new Promise((resolve, reject) =>{
        var query = {"uuid._id" : uuid};
        var options = {};
        collection.findOne(query, options)
        .then((data) => {
            console.log("Data : ", data);
            resolve(data);
        }).catch(err => {
            console.log("err : ", data);
            reject(data);
        }) 
    })
    
}

const updateAnalytics = (collection, payload, uuid) => {
    var query = {uuid : {_id : uuid}};
    var updateDoc = {$set: payload}
    var options = {};
    collection.updateOne(query, updateDoc, options)
        .then(res => {
            console.log("Update success to Id : %s"+ uuid,  res);
            return Promise.resolve();
        }).catch(err =>{
            console.log("Update failed to Id : `%s`"+uuid, err);
            return Promise.reject();
        })
}

module.exports.testAnalyticsDao = {
    connectMongoDb : connectMongoDb,
    testAnalyticsDashboard : dashboardCollection,
    updateAnalytics : updateAnalytics,
    readAnalytics : readAnalytics
}