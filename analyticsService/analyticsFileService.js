var fetch = require("node-fetch");
var csvToJson = require("csvtojson");

const readExperienceKpi = async (url, kpiName) => {
  const response = await fetch(url, { method: "GET" });
  const data = await response.text();

  let parsedJson = await csvToJson({
    noheader: false,
    colParser:{
      "column1":"omit"
  },
    output: "json",
  }).fromString(data);

  return { [kpiName] : parsedJson };
};

const getAllExperienceKpi = (
  cpuUsageUrl,
  memoryUsageUrl,
  batteryUsageUrl,
  heapMemoryUsageUrl,
  graphicsUrl
) => {
  return new Promise((resolve, reject) => {
    const kpiPromises = new Array();
    const readCPUKpi = cpuUsageUrl ? readExperienceKpi(cpuUsageUrl, "cpu") : null;
    const readMemoryKpi = memoryUsageUrl
      ? readExperienceKpi(memoryUsageUrl, "memory")
      : null;
    const readBatteryKpi = batteryUsageUrl
      ? readExperienceKpi(batteryUsageUrl, "battery")
      : null;
    const readHeapMemoryKpi = heapMemoryUsageUrl
      ? readExperienceKpi(heapMemoryUsageUrl, "HEAPMEMORYKPI")
      : null;
    const readFramesKpi = graphicsUrl
      ? readExperienceKpi(graphicsUrl, "graphics")
      : null;

    if (readCPUKpi) kpiPromises.push(readCPUKpi);
    if (readMemoryKpi) kpiPromises.push(readMemoryKpi);
    if (readBatteryKpi) kpiPromises.push(readBatteryKpi);
    if (readHeapMemoryKpi) kpiPromises.push(readHeapMemoryKpi);
    if (readFramesKpi) kpiPromises.push(readFramesKpi);

    console.log("Promises Array : ", kpiPromises.length);

    Promise.allSettled(kpiPromises)
      .then(results => {
        console.log(results);
        let kpisJson = new Object();
        for(let result of results ){
          let value = result.value;
          let kpiName = Object.keys(value)[0];
          kpisJson[kpiName] = value[kpiName];
        }
        
        //console.log("KPIS : ",kpisJson);
        resolve(kpisJson);})
  });
};

module.exports.fileService = {
  readExperienceKpi: readExperienceKpi,
  getAllExperienceKpi : getAllExperienceKpi
};
