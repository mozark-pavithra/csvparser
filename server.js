var express = require('express');
const { Collection } = require('mongodb');
var app = express();

var testAnalytics = require('./analyticsService/testAnalyticsDao').testAnalyticsDao;
var analyticsFile = require('./analyticsService/analyticsFileService').fileService;

process.port =  3000; 

app.listen(process.port);
app.on('connect', ()=> {
    console.log("testAnalytics is up and running on Port : ", process.port);
});


app.post('/analytics', (req,res) => {
    var uuid = 45203545; // to change it as a request parameter..

    var dashboard;
    testAnalytics.connectMongoDb()
    .then(() => {
        return testAnalytics.testAnalyticsDashboard()
    }).then (collectionObj => {
            dashboard = collectionObj;
            //console.log("dashboard : ", dashboard);
            return testAnalytics.readAnalytics(dashboard,uuid);
    }).then((testCase) => {
        //console.log(testCase);
        var memoryLogUrl = testCase['Experience_Logs'].testExecutionMemoryUsageURL;
        var cpuUsageUrl = testCase['Experience_Logs'].testExecutionCPUUsageURL;
        var batteryUsageUrl = testCase['Experience_Logs'].testExecutionBatteryUsageURL;
        var heapMemoryUsageUrl = testCase['Experience_Logs'].testExecutionHeapMemoryURL;
        var graphicsUrl = testCase['Experience_Logs'].testExecutionFrameURL;
        
        return analyticsFile.getAllExperienceKpi(cpuUsageUrl, memoryLogUrl, batteryUsageUrl, heapMemoryUsageUrl, graphicsUrl);
    }).then(result => {
            console.log("resultReceived");
            result.uuid_id = uuid;
            res.status(200).send({"message" : "success", "data" : result})
    }).catch(err => {
            console.log("Server : ", err);
            res.status(400).send({"message" : "failure"})
    })
})


module.exports = app;